# Data Exchange Services Specifications

## Document Purpose

The purpose of the document is to specify Data Exchange Services, including high level architecture and key requirements for data value, trust and compliance. 

## Data Exchange Services

- Data Products catalogue
- Data Usage Agreement
- Data Access Logging

## Data Product Conceptual Model and Data Usage Operational Models

The `Data Product` conceptual model and the `Data Usage` operational model are described in the `Data Exchange Services` chapter of the `Gaia-X Architecture Document`.

##Data Products Catalog

- Include ontologies / vocabulary / semantics 

## Data Usage Agreement

## Data Access Loging

## Annex
