# Data Exchange Document 25.xx

## Publisher

Gaia-X European Association for Data and Cloud AISBL  
Avenue des Arts 6-9  
1210 Brussels  
www.gaia-x.eu

## Authors

Gaia-X European Association for Data and Cloud AISBL

## Contact

https://gaia-x.eu/contact/

## Copyright notice

©2023 Gaia-X European Association for Data and Cloud AISBL

This document is protected by copyright law and international treaties. This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/). Third party material or references are cited in this document.

![cc by-nc-nd](media/by-nc-nd.eu.png)


<script src="https://unpkg.com/mermaid@8.13.2/dist/mermaid.min.js"></script>
<script>
function fix2124(id) {
    // fix issue https://github.com/mermaid-js/mermaid/issues/2124
    document.querySelectorAll("#" + id + " marker[markerWidth]").forEach((elt) => {
        elt.setAttribute("markerWidth", elt.getAttribute("markerWidth") > 100 ? elt.getAttribute("markerWidth")/10 : elt.getAttribute("markerWidth"))
    });
    document.querySelectorAll("#" + id + " marker[markerHeight]").forEach((elt) => {
        elt.setAttribute("markerHeight", elt.getAttribute("markerHeight") > 100 ? elt.getAttribute("markerHeight")/10 : elt.getAttribute("markerHeight"))
    });
}

mermaid.initialize({
    startOnLoad: true,
    mermaid: {
        callback: function(id) {
            console.log("mermaid callback with id", id);
            fix2124(id);
            let svg = document.querySelector('#' + id);
            let data = new XMLSerializer().serializeToString(svg);
            let body = {svg: "data:image/svg+xml;base64," + window.btoa(data)};
            console.log(body)
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "https://svg2png.riphixel.fr/convert/svg2png", false);  // synchronous request
            xhr.send(JSON.stringify(body));
            console.log(xhr.responseText);
            let img = new Image();
            img.src = xhr.responseText.png;
            svg.parentNode.appendChild(img);
            svg.remove();
        }
    },
    theme: "base",
    themeVariables: {"primaryColor": "#CCE4FF", "edgeLabelBackground": "#ffffff", "tertiaryColor": "#b900ff11", "tertiaryTextColor": "#000", "tertiaryBorderColor": "#b900ff"}, // using https://filosophy.org/code/online-tool-to-lighten-color-without-alpha-channel/ to remove alpha channel from standard 4 colors
    themeCSS: ".label, .cluster-label, .actor, .messageText, .noteText, .loopText, .labelText {font-family: 'Titillium Web' !important; font-size: 10pt !important} .cluster-label {font-weight: 700}", // tested using https://mermaid.live/
    logLevel: 3 // warning
});
</script>
