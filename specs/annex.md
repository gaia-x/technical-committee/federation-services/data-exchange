## Ontologies for Data Exchange

### Participants

The `Data Exchange` conceptual model specify particular roles for the participant of a Gaia-X Ecosystem. When possible, the corresponding role/type in the Gaia-X Conceptual Model and Trust Framework is précised.
The role has an importance when defining the following Data Objects and rules.

| Object | SubclassOf | Comment |
| ------ | ---------- | ------- |
| DataProductProvider | ['gx:LegalPerson'] | is equivalent to Gaia-X Provider.|
| DataLicensor | ['gx:LegalPerson'] | is equivalent to Gaia-X Licensor. |
| DataProducer | ['gx:LegalPerson'] | is equivalent to Gaia-X RessourceOwner. |
| DataConsumer | ['gx:LegalPerson'] | is equivalent to Gaia-X Consumer. |

The attributes for the different participant are of type ['gx:LegalPerson'] as defined in the TrustFramework https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/participant/#legal-person

<!-- do we delete this, if the attributes are defined elsewhere? -->

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| [`parentOrganisation`](https://schema.org/parentOrganization) | participant[] | No | A list of direct `participant` that this entity is a subOrganization of, if any.  | 
| `name` | String | Yes | Name of `participant`. | 
| `registrationNumber` | String | Yes | Country’s registration number which identifies one specific company. |
| `LEI Code` | String | No | Unique LEI number as defined by https://www.gleif.org. |
| `headquarterAddress` | String  | Yes | Physical location of head quarter in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| `headquarterAddress.street-address` | String  | No | Street Address. |
| `headquarterAddress.postal-code` | String  | No | Postal Code. |
| `headquarterAddress.region` | String | No | Region. | 
| `headquarterAddress.locality` | String | No | Locality. | 
| `headquarterAddress.country-name` | String | Yes | Country Name. |
| `legalAddress` | String | Yes | Physical location of legal quarter in ISO 3166-1 alpha2, alpha-3 or numeric format. |
| `legalAddress.street-address` | String  | No | Street Address. |
| `legalAddress.postal-code` | String  | No | Postal Code.  |
| `legalAddress.region` | String | No | Region. | 
| `legalAddress.locality` | String | No | Locality. | 
| `legalAddress.country` | String | Yes | Country. |

### Data Product

A `Data Product` consists of the characterization of the actual data as well a description of the contractual part. At minimum, a `Data Product Description` needs to contain all information so that a `Data Consumer` can initiate a contract negotiation. All other attributes that are used to describe the data are optional. However, the `Data Provider` has an interest to precisely describe the data so that it can be found and consumed. If a `Data Product` is published in a catalogue, the `Data Provider` might precisely describe the `Data Product Description` so that it can be found and consumed by `Data Consumers`.

A `Data Product Description` includes a list of one or many `Dataset` which includes a list of one or many `Distribution`.

#### Data Product Description

A `Data Product Description` is inheriting the attributes of a `Service Offering` as defined by the Trust Framework  [Service Offering](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/service_and_subclasses/#service-offering), and also extending the [DCAT-3 Cataloged Resource class](https://www.w3.org/TR/vocab-dcat-3/#Class:Resource).


| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `gx:providedBy` | URI | Yes | A resolvable link to the participant Description providing the service. | 
| `gx:termsAndConditions` | URI | Yes | A resolvable link to the Terms and Conditions applying to that service. Terms and conditions may include Business Conditions and SLA's that apply to the `Data Product`. |
| `dct:license`  | String[] | Yes | A list of URIs to license documents. |
| `dct:title` or `gx:title` | String |Yes | Title of the Data Product. |
| `dct:description` or `gx:description` | String | No | Description of the Data Product. |
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:obsoleteDateTime`   | Date - ISO 8601 | No | Date time in ISO 8601 format after which the Data Product is obsolete. |
| `odrl:hasPolicy`      | policy in ODRL | No | `Policy` expressed using ODRL. |
| `gx:dataLicensors`     | URI[] | No | A list of Licensors either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |
| `gx:aggregationOf`  | dcat:dataset[] or gx:dataset[] | Yes | DataSet Content. |
| `dct:identifier`  | String | Yes | Unique uuid4. |
| `dcat:contactPoint` | Format vCard [VCARD-RDF] | No | Contacts to get more information. |
| `dcterms:conformsTo` | URI[] |  No | A list of established standards to which the described resource conforms. |

**Note: Terms and Data Usage Agreement are two distinct concepts.** 

The `gx:termsAndConditions` are mandatory to establish the way the service is consumed and contracted. In addition, the `gx:dataUsageAgreement` is optional and describes the list of authorizations from the `Data Rights Holder`.
In case of PII, the Data Controller Participants (as defined in GDPR) are listed under `gx:dataLicensors`.

**Note:** If Terms and Conditions include Business Conditions and/or SLA's, it is the responsibility of the Data Provider to provide ODRL descriptions for these terms, for them to be automatically checked. 

**Note: Domain Specific Attributes**
Additional attributes that are important to describe a Data Product but should be defined in relation with an application domain's needs to be defined by Ecosystems by extending the model per Data Domain and not at the Gaia-X level. As an example, `dcat:theme` or `dcat:keyword` requires a specific domain vocabulary. It has been defined at the Ecosystem Level. 

It is recommended to extend a `Data Product Description` at least by the following attributes:

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `dcat:keywords` | String[] | No | List of Tags or Keywords (Unicode) for data domains. |
| `dcat:theme` | String[] | No | A main category of the resource. A resource can have multiple themes. |
| `dcterms:type` | String[] | No | The nature or genre of the resource. |

**Consistency rules**

- The keypair used to sign the Data Resource claims must be traceable to the `gx:producedBy` participant of the Data Resource.
- If the data are about data subjects as one or more Natural Persons, or sensitive data as defined in GDPR [article 9](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN#d1e2051-1-1), than `dataController` and `DataUsageAgreement` are mandatory. 
<!-- `dataController` and `Agreement` are not listed in the table. Should they be added? --> 
  To avoid [data re-identification](https://ec.europa.eu/eurostat/cros/content/re-identification_en), this rule applies independently if the data is raw, pseudo-anonymized or anonymized. (Note: This is on purpose beyond GDPR requirements.)
- If `gx:dataLicensors` is specified, the `Data Rights Holder` should be the issuer of the Data Resource `gx:dataUsageAgreement`. Claims must be traceable and verifiable against Gaia-X Compliance. 

#### Dataset Description

A `Dataset` is extending the attributes of [DCAT-3 Dataset class](https://www.w3.org/TR/vocab-dcat-3/#Class:Dataset).
 
| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------| ------ |
| `dct:title`| String |Yes | Title of the Data Product. |
| `dct:distributions` | dcat:distribution[] | Yes | List of distributions format of the dataset. |
| `dct:identifier`  | String | Yes | Unique uuid4. |
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:expirationDateTime` | Date - ISO 8601 | No  | Date time in ISO 8601 format after which data is expired and shall be deleted. |
| `dct:license`  | String[] | No | A list of URIs to license document. |
| `gx:dataLicensors`     | URI[] | No | A list of Data Rights Holders either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |
| `gx:exposedThrough`  | URI[] | Yes | A resolvable link to the data exchange component that exposes the Data Product. |

#### Distribution Description

A `Distribution` is extending the attributes of [DCAT-3 Distribution class](https://www.w3.org/TR/vocab-dcat-3/#Class:Distribution).

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | -------|
| `dct:title`| String | Yes | Title of the Data Product. |
| `dct:format` | Standard IANA | Yes | Format of the dataset distribution (pdf, csv, …). |
| `dcat:compressFormat` | Standard IANA | No | The compression format of the distribution in which the data is contained in a compressed form. |
| `dcat:packageFormat` | Standard IANA | No | The package format of the distribution in which one or more data files are grouped together. |
| `dcat:byteSize` | String | No | Size of the dataset distribution. |
| `gx:location` | String[] | No | List of dataset storage location. |
| `gx:hash` | String | No | To uniquely identify the data contained in the dataset distribution.|
| `gx:hashAlgorithm` | String | No | Hash Algorithm.|
| `dct:issued` | Date - ISO 8601 | No | Publication date in ISO 8601 format.|
| `gx:expirationDateTime` | Date - ISO 8601 | No  | Date time in ISO 8601 format after which data is expired and shall be deleted. |
| `dcat:language`	 | Lang : ISO 639-1:2002 | No | Language.  |
| `dct:license`  | String[] | No | A list of URIs to license document. |
| `gx:dataLicensors`     | URI[] | No | A list of Licensors either as a free form string or `participant` Description.|
| `gx:dataUsageAgreement` | DataUsageAgreement[]  | No  | List of authorizations from the data subjects as Natural Person when the dataset contains PII, as defined by the Trust Framework. |

#### Data Catalogue

In Gaia-X Trust Framework, a `catalogue` is a subclass of a `Service Offering` with one additional attribute providing the route to retrieve the list of Verifiable Credentials IDs [Catalogue](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/service_and_subclasses/#catalogue).

As such, a `Data Catalogue` directly inherits the `Catalogue` class.

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | -------|
| `gx-trust-framework:getVerifiableCredentialsIDs`| String | Yes | A route used to synchronize catalogues and retrieve the list of Verifiable Credentials (issuer, id). |

In DCAT, a catalogue is a list of catalog records [service, dataset, resources, catalogue] descriptions [DCAT-3 Catalog class](https://www.w3.org/TR/vocab-dcat-3/#Class:Catalog).

If all the records of a DCAT Catalog are exposed as Verifiable Credentials through IDs in a Gaia-X Catalogue, then both object are conceptually similar.

### Data Transaction

Data Transaction is comprised of a `Data Product Usage Contract` and a `Data Usage`.

A `Data Product Usage Contract` is based on the `Data Product Description` but may possibly differ from the original one after modification during the negotiations. It is signed by both `Data Provider` and `Data Consumer`. It must include `Terms of Usage`. For each `Dataset` included in the `Data Product`, the `Data Product Usage Contract` must include an explicit `Data Usage Agreement`.

The `Data Product Usage Contract` is a Ricardian contract. The parties can (optionally) request this contract to be notarized in a `Data Product Usage Contract Store`. 

After such contract has been agreed upon and has been signed by both parties, the `Data Consumer` can start accessing and using the data, realizing the `Data Product Usage Contract`. Such `Data Usage` with the associated `Data Product Usage Contract` corresponds to a `Data Transaction`.

The contract negotiation can lead to both parties agreeing on a `Data Usage Logging Service`.

`Signature Check Type`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:participantRole` | String | Yes | Establish a unique way to identify the participant that has to Sign (e.g. `gx:providedBy` is identified by `Provider` ). Possible values are Provider, Consumer, Licensor, Producer. | 
| `gx:mandatory` | String | Yes | Establish  if a Signature is mandatory or Optional. Possible values are Yes/No. | 
| `gx:legalValidity` | String| Yes | Establish if the legal validity check needs to be enforced to the Signature. Possible values are Yes/No. |

`Data Product Usage Contract`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:providedBy` | URI | Yes | A resolvable link to the `Data Provider`. | 
| `gx:consumedBy` | URI | Yes | A resolvable link to the `Data Consumer`. | 
| `gx:dataProduct` | URI | Yes | A resolvable link to the `Data Product Description` (after negotiation). |
| `gx:signers` | SignatureCheckType[]| Yes | The array identifying all required Participant signatures.|
| `gx:termOfUsage` | URI | Yes | A resolvable link to the Term of Usage. |
| `gx:notarizedIn` | URI | No | A resolvable link to the Notarization service. |
| `odrl:hasPolicy`  | policy in ODRL | No | `Policy` Agreement expressed using ODRL. |

`Data Usage`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:loggingService` | ServiceOffering | No | Logging Service. |
| `gx:dataUsageContract` | URI | Yes | A resolvable link to Data Usage Contract. | 


**Note:** Logging Service shall monitor significant parameters and history logs in order to enforce Terms and Conditions (Business Conditions and SLA's) corresponding (if defined) to the ORDL policy (territories, industries, usages, expiration, ...). 

`Data Usage Agreement`

| Attribute | Type.Value/Voc | Mandatory | Comment |
| ------ | ------ | ------ | ------ |
| `gx:producedBy` | URI | Yes | A resolvable link to the `Data Producer`. | 
| `gx:providedBy` | URI | Yes | A resolvable link to the `Data Provider`. | 
| `gx:licensedBy` | URI[] | No | A list of resolvable links to `Data Rights Holders`. | 
| `gx:dataUsageAgreementTrustAnchor` | URI | Yes | A resolvable link to the `Data Usage Agreement Trust Anchor`. | 
| `gx:dataProduct` | URI | Yes | A resolvable link to the `Data Product Description`. |
| `gx:signers` | SignatureCheckType[]| Yes | The array identifying all required Participant signatures.|

